#!/bin/bash

python makeLogs.py
mkdir -p pdfs
cd pdfs
for f in log*.tex
do
	pdflatex $f
done
pdfunite log*.pdf allLogs.pdf
